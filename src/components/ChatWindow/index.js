import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import {
  cleanupChats,
  changeMessage,
  sendMessage,
} from "../../actions/actions";
import styles from "./chat.module.scss";

const ChatWindow = ({
  chatName,
  message,
  messages,
  cleanupChats,
  changeMessage,
  sendMessage,
}) => {
  const { id } = useParams();
  useEffect(() => {
    return () => {
      cleanupChats();
    };
  }, []);

  const handleChange = (event) => {
    changeMessage(event.target.value);
  };

  const handleClick = () => {
    sendMessage(message);
  };

  return (
    <div className={styles.window}>
      <div className={styles.header}>
        <div className={styles.name}>{chatName}</div>
      </div>
      <div className={styles.messages}>
        {messages.map((message) => {
          return <div>{message}</div>;
        })}
      </div>
      <div className={styles.inputBox}>
        <input
          onChange={handleChange}
          className={styles.msgInput}
          placeholder="Type your message here"
          type="text"
          value={message}
        />
        <button className={styles.btn} onClick={handleClick}>
          SEND
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    chatName: state.chats.chatName,
    message: state.chats.message,
  };
};

export default connect(mapStateToProps, {
  cleanupChats,
  changeMessage,
  sendMessage,
})(ChatWindow);
