import React from "react";
import styles from "./squares.module.scss";

const SquaresBackground = () => {
  return (
    <div className={styles.wrapper}>
      <ul className={styles.bubbles}>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
      
    </div>
  );
};

export default SquaresBackground;
