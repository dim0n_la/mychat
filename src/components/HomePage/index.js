import React from "react";
import styles from "./home.module.scss";

const HomePage = () => {
  return (
    <div className="l-content">
      <div className={styles.container}>
        <div className={styles.title}>
          Greetings, this is MyChat 
          <span role="img" aria-label="cyclone">
            🌀
          </span>
        </div>
        <div className={styles.subtitle}>
          You should either login or signup to continue
        </div>
      </div>
    </div>
  );
};

export default HomePage;
