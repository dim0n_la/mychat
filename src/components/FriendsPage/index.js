import React, { useEffect } from "react";
import { connect } from "react-redux";
import styles from "./friends.module.scss";
import InfoMessage from "../InfoMessage/index";
import FriendsList from "../FriendsList/index";
import {
  updateFriendTag,
  checkIfTagExists,
  addFriend,
  fetchFriends,
} from "../../actions/actions";

const FriendsPage = ({
  updateFriendTag,
  checkIfTagExists,
  tag,
  formValid,
  friends,
  addFriend,
  fetchFriends,
}) => {
  useEffect(() => {
    fetchFriends();
  }, [fetchFriends]);

  const handleTagChange = (event) => {
    updateFriendTag(event.target.value);
  };
  const handleInputBlur = () => {
    checkIfTagExists(tag);
  };

  const handleClick = (event) => {
    event.preventDefault();
    addFriend(tag);
  };

  return (
    <div className="l-content">
      <div className={styles.container}>
        <div className={styles.title}>
          Here is the the list of all your friends, you can add new ones below
          <span role="img" aria-label="running person">
            🏃‍♀️
          </span>{" "}
          <span role="img" aria-label="running person">
            🏃
          </span>{" "}
          <span role="img" aria-label="running person">
            🏃‍♂️
          </span>{" "}
          <span role="img" aria-label="running person">
            🏃‍♀️
          </span>
        </div>
        <FriendsList friends={friends} />
        <form className={styles.form}>
          <label htmlFor="friendTag">
            Please, enter your friend's tag below:
          </label>
          <input
            onBlur={handleInputBlur}
            onChange={handleTagChange}
            className={styles.input}
            type="text"
            id="friendTag"
            placeholder="tag"
            value={tag}
          />
          {!formValid && (
            <InfoMessage message="This tag does not exist" isError={true} />
          )}
          <button
            onClick={handleClick}
            type="submit"
            className={`${styles.btn} ${formValid ? "" : styles.disabled}`}
          >
            Add Friend
          </button>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    tag: state.friends.tag,
    formValid: state.friends.formValid,
    friends: state.friends.friends,
  };
};

export default connect(mapStateToProps, {
  updateFriendTag,
  checkIfTagExists,
  addFriend,
  fetchFriends,
})(FriendsPage);
