import React from "react";
import { connect } from "react-redux";
import styles from "./progress.module.scss";

const ProfileProgress = ({ progress, progressMessage }) => {
  return (
    <div className={styles.container}>
      <label className={styles.label} for="progress">
        {progressMessage}
      </label>
      <progress id="progress" value={progress} max="100">
        {`${progress}%`}
      </progress>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    progress: state.userSettings.progress,
    progressMessage: state.userSettings.progressMessage,
  };
};

export default connect(mapStateToProps, null)(ProfileProgress);
