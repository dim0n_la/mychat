import React from "react";
import styles from "./card.module.scss";
import { Link } from "react-router-dom";
//todo: a bug: chat name is not being set if coming from the friend list

const FriendCard = ({ friend }) => {
  return (
    <>
      <div className={styles.container}>
        <div className={styles.info}>
          <div className={styles.image}>
            <img src={friend.photoLink} alt="a friend" />
          </div>
          <div className={styles.text}>
            <div className={styles.name}>{friend.name}</div>
            <div>{friend.tag}</div>
          </div>
        </div>
        <div className={styles.btns}>
          <div className={styles.btn}>Remove</div>
          <Link className={styles.btn} to={`chats/${friend.chatID}`}>
            Send a message
          </Link>
        </div>
      </div>
    </>
  );
};

export default FriendCard;
