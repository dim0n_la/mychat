import React from "react";
import styles from "./info-message.module.scss";

const InfoMessage = ({ message, isError }) => {
  return (
    <div className={`${styles.message} ${isError ? styles.error : ""}`}>
      {message}
    </div>
  );
};

export default InfoMessage;
