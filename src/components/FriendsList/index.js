import React from "react";
import InfoMessage from "../InfoMessage/index";
import FriendCard from "../FriendCard/index";

const FriendsList = ({ friends }) => {
  if (friends.length === 0) {
    return (
      <InfoMessage
        isError={false}
        message="You do not have any friends listed yet."
      />
    );
  }
  return friends.map((friend) => {
    return <FriendCard key={friend.tag} friend={friend} />;
  });
};

export default FriendsList;
