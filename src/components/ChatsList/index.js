import React from "react";
import InfoMessage from "../InfoMessage/index";
import ChatCard from "../ChatCard/index";

const ChatsList = ({ chats }) => {
  if (chats.length === 0) {
    return (
      <div>
        <InfoMessage
          isError={false}
          message="You do not have any chats listed yet."
        />
      </div>
    );
  }
  return (
    <div>
      {chats.map((chat) => {
        let cardData;
        for (let prop in chat.users) {
          if (!chat.users[prop].current) {
            cardData = chat.users[prop];
          }
        }
        return (
          <ChatCard
            key={chat.chatID}
            chatID={chat.chatID}
            name={cardData.name}
            imgsrc={cardData.photoLink}
          />
        );
      })}
    </div>
  );
};

export default ChatsList;
