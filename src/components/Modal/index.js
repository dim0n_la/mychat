import React from "react";
import { connect } from "react-redux";
import Form from "../Form/index";
import { closeModal } from "../../actions/actions";
import styles from "./modal.module.scss";

const Modal = ({ showModal, closeModal, resetForm }) => {
  if (!showModal) {
    return null;
  }

  const handleBgClick = () => {
    closeModal();
  };

  return (
    <div onClick={handleBgClick} className={styles.background}>
      <Form inModal={true} />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    showModal: state.modal.showModal,
  };
};

export default connect(mapStateToProps, { closeModal })(Modal);
