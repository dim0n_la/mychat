import React from "react";
import { useRouteMatch } from "react-router-dom";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { changeChatName } from "../../actions/actions";
import styles from "./chat-card.module.scss";

const ChatCard = ({ imgsrc, name, chatID, changeChatName }) => {
  const { path, url } = useRouteMatch();
  const handleClick = () => {
    changeChatName({ name: name, chatID: chatID });
  };

  return (
    <Link
      onClick={handleClick}
      to={`${url}/${chatID}`}
      className={styles.container}
    >
      <div className={styles.image}>
        <img src={imgsrc} alt="profile" />
      </div>
      <div className={styles.name}>{name}</div>
    </Link>
  );
};

export default connect(null, { changeChatName })(ChatCard);
