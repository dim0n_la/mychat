import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { modalReducer } from "../reducers/modalReducer";
import { formReducer } from "../reducers/formReducer";
import { userStatusReducer } from "../reducers/userStatusReducer";
import { userSettingsReducer } from "../reducers/userSettingsReducer";
import { friendsListReducer } from "../reducers/friendsListReducer";
import { chatsReducer } from "../reducers/chatsReducer";

const customizedMiddleware = getDefaultMiddleware({
  immutableCheck: {
    ignoredPaths: ["userStatus"],
  },
});

export default configureStore({
  reducer: {
    friends: friendsListReducer,
    chats: chatsReducer,
    modal: modalReducer,
    form: formReducer,
    userStatus: userStatusReducer,
    userSettings: userSettingsReducer,
  },
  middleware: [...customizedMiddleware],
});
