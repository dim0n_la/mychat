import * as actiontypes from "../actions/actiontypes";
import { createReducer } from "@reduxjs/toolkit";

export const userStatusReducer = createReducer(
  { isAuthenticated: false, user: null },
  {
    [actiontypes.SET_USER_STATUS_LOGIN]: (state, action) => {
      state.isAuthenticated = true;
      state.user = action.payload;
    },
    [actiontypes.SET_USER_STATUS_SIGNOUT]: (state, action) => {
      state.isAuthenticated = false;
      state.user = null;
    },
  }
);
