import * as actiontypes from "../actions/actiontypes";
import { createReducer } from "@reduxjs/toolkit";

export const formReducer = createReducer(
  {
    type: "login",
    buttonText: "",
    titleText: "",
    hasError: false,
    errorMessage: "",
    email: "",
    password: "",
  },
  {
    [actiontypes.SHOW_LOGIN]: (state, action) => {
      state.type = "login";
      state.buttonText = "Log in";
      state.titleText = "Please, log in";
    },
    [actiontypes.SHOW_SIGNUP]: (state, action) => {
      state.type = "signup";
      state.buttonText = "Sign up";
      state.titleText = "Please, sign up";
    },
    [actiontypes.CHANGE_EMAIL]: (state, action) => {
      state.email = action.payload;
    },
    [actiontypes.CHANGE_PASSWORD]: (state, action) => {
      state.password = action.payload;
    },
    [actiontypes.LOGIN_USER_SUCCESS]: (state, action) => {
      state.hasError = false;
      state.errorMessage = "";
      state.email = "";
      state.password = "";
    },
    [actiontypes.LOGIN_USER_ERROR]: (state, action) => {
      state.hasError = true;
      state.errorMessage = action.error.message;
      state.email = "";
      state.password = "";
    },
    [actiontypes.SIGNUP_USER_SUCCESS]: (state, action) => {
      state.hasError = false;
      state.errorMessage = "";
      state.email = "";
      state.password = "";
    },
    [actiontypes.SIGNUP_USER_ERROR]: (state, action) => {
      state.hasError = true;
      state.errorMessage = action.error.message;
      state.email = "";
      state.password = "";
    },
    [actiontypes.RESET_FORM]: (state, action) => {
      state.type = "login";
      state.buttonText = "";
      state.titleText = "";
      state.hasError = false;
      state.errorMessage = "";
      state.email = "";
      state.password = "";
    },
  }
);
