import * as actiontypes from "../actions/actiontypes";
import { createReducer } from "@reduxjs/toolkit";

export const friendsListReducer = createReducer(
  {
    tag: "",
    formValid: true,
    friends: [],
  },
  {
    [actiontypes.UPDATE_FRIEND_TAG]: (state, action) => {
      state.tag = action.payload;
    },
    [actiontypes.CHECK_IF_TAG_EXISTS_SUCCESS]: (state, action) => {
      if (action.payload) {
        state.formValid = false;
      } else {
        state.formValid = true;
      }
    },
    [actiontypes.ADD_FRIEND_SUCCESS]: (state, action) => {
      state.friends.push(action.payload);
    },
    [actiontypes.FETCH_FRIENDS_SUCCESS]: (state, action) => {
      if (action.payload !== undefined) {
        state.friends = action.payload;
      }
    },
    [actiontypes.CLEANUP_FRIENDS]: (state, action) => {
      state.friends = [];
      state.tag = "";
      state.formValid = true;
    },
  }
);
