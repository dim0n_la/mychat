import * as actiontypes from "../actions/actiontypes";
import { createReducer } from "@reduxjs/toolkit";

export const chatsReducer = createReducer(
  {
    chats: [],
    activeChatID: "",
    activeChatMessages: [],
    chatName: "",
    message: "",
  },
  {
    [actiontypes.FETCH_CHATS_SUCCESS]: (state, action) => {
      state.chats = action.payload;
    },
    [actiontypes.CHANGE_CHAT_NAME]: (state, action) => {
      state.chatName = action.payload.name;
      state.activeChatID = action.payload.chatID;
    },
    [actiontypes.CHANGE_MESSAGE]: (state, action) => {
      state.message = action.payload;
    },
    [actiontypes.SEND_MESSAGE]: (state, action) => {
      let currentChat = state.chats.filter((chat) => {
        return chat.chatID === state.activeChatID;
      });
      currentChat[0].messages.push(action.payload);
      state.activeChatMessages.push(action.payload);
      state.message = "";
    },
    [actiontypes.CLEANUP_CHATS]: (state, action) => {
      state.chats = [];
      state.chatName = "";
      state.message = "";
    },
  }
);
