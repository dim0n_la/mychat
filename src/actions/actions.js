import * as actiontypes from "./actiontypes";
import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import {
  addUserInfo,
  addUserToFriendsList,
  getUserByTag,
  getUserFriends,
  getUserInfo,
  postUserSettings,
  postUserTag,
  putImage,
  tagExists,
  userLogin,
  userSignout,
  userSignup,
  createChat,
  getCurrentUserInfo,
  getUserChats,
} from "../controllers/firebaseController";

// Modal actions
export const openModal = createAction(actiontypes.OPEN_MODAL);
export const closeModal = createAction(actiontypes.CLOSE_MODAL);

// // Sign up and log in form actions
export const resetForm = createAction(actiontypes.RESET_FORM);
export const showSignup = createAction(actiontypes.SHOW_SIGNUP);
export const showLogin = createAction(actiontypes.SHOW_LOGIN);
export const changeEmail = createAction(actiontypes.CHANGE_EMAIL);
export const changePassword = createAction(actiontypes.CHANGE_PASSWORD);
export const signoutUser = createAsyncThunk(
  actiontypes.SIGNOUT_USER,
  async (arg, thunkAPI) => {
    userSignout();
  }
);
export const loginUser = createAsyncThunk(
  actiontypes.LOGIN_USER,
  async (arg, thunkAPI) => {
    const user = await userLogin(arg.email, arg.password);
    return user;
  }
);
export const loginUserWithModal = createAsyncThunk(
  actiontypes.LOGIN_USER,
  async (arg, thunkAPI) => {
    const user = await userLogin(arg.email, arg.password);
    thunkAPI.dispatch(closeModal());
    return user;
  }
);
export const signupUser = createAsyncThunk(
  actiontypes.SIGNUP_USER,
  async (arg, thunkAPI) => {
    const userCreds = await userSignup(arg.email, arg.password);
    const UID = userCreds.user.uid;
    await addUserInfo(UID, { email: userCreds.user.email });
    thunkAPI.dispatch(closeModal());
    return userCreds;
  }
);

// User settings actions
export const changeName = createAction(actiontypes.CHANGE_NAME);
export const changePhone = createAction(actiontypes.CHANGE_PHONE);
export const changeTag = createAction(actiontypes.CHANGE_TAG);
export const changeInfo = createAction(actiontypes.CHANGE_INFO);
export const uploadImage = createAction(actiontypes.UPLOAD_IMAGE);
export const checkTagUniqueness = createAsyncThunk(
  actiontypes.CHECK_TAG_UNIQUENESS,
  async (arg, thunkAPI) => {
    const isUnique = await tagExists(arg);
    return isUnique;
  }
);

export const postSettings = createAsyncThunk(
  actiontypes.POST_SETTINGS,
  async (arg, thunkAPI) => {
    if (arg.tag) {
      await postUserTag(arg.tag);
    }
    const result = await postUserSettings({
      email: arg.email,
      name: arg.name,
      phone: arg.phone,
      tag: arg.tag,
      info: arg.info,
    });
    return result;
  }
);

export const postSettingsWithPhoto = createAsyncThunk(
  actiontypes.POST_SETTINGS_WITH_PHOTO,
  async (arg, thunkAPI) => {
    const imageDownloadURL = await putImage(arg.photo);
    if (arg.tag) {
      await postUserTag(arg.tag);
    }
    const result = await postUserSettings({
      email: arg.email,
      name: arg.name,
      phone: arg.phone,
      tag: arg.tag,
      info: arg.info,
      photoLink: imageDownloadURL,
    });
    return result;
  }
);

export const fetchProfileInfo = createAsyncThunk(
  actiontypes.FETCH_PROFILE_INFO,
  async (arg, thunkAPI) => {
    const userInfo = await getUserInfo(arg.email);
    return userInfo;
  }
);
export const cleanupProfileInfo = createAction(
  actiontypes.CLEANUP_PROFILE_INFO
);

// Friends actions
export const updateFriendTag = createAction(actiontypes.UPDATE_FRIEND_TAG);
export const checkIfTagExists = createAsyncThunk(
  actiontypes.CHECK_IF_TAG_EXISTS,
  async (arg, thunkAPI) => {
    const isUnique = await tagExists(arg);
    return isUnique;
  }
);
export const addFriend = createAsyncThunk(
  actiontypes.ADD_FRIEND,
  async (arg, thunkAPI) => {
    const user = await getUserByTag(arg);
    const currentUser = await getCurrentUserInfo();
    const chatID = await createChat(
      currentUser.userData.name,
      currentUser.userData.photoLink,
      user.name,
      user.photoLink
    );
    const userDataInList = await addUserToFriendsList(user, chatID);
    return userDataInList;
  }
);
export const fetchFriends = createAsyncThunk(
  actiontypes.FETCH_FRIENDS,
  async (arg, thunkAPI) => {
    const friends = await getUserFriends();
    return friends;
  }
);

//Chats actions
export const fetchChats = createAsyncThunk(
  actiontypes.FETCH_CHATS,
  async (arg, thunkAPI) => {
    let currentUserName = await getCurrentUserInfo();
    const chats = await getUserChats();
    //todo: see if I can get a workaround for the 2 x loop
    for (let chat of chats) {
      for (let prop in chat.users) {
        chat.users[prop].current =
          chat.users[prop].name === currentUserName.userData.name;
      }
    }
    return chats;
  }
);
export const changeChatName = createAction(actiontypes.CHANGE_CHAT_NAME);
export const cleanupChats = createAction(actiontypes.CLEANUP_CHATS);
export const changeMessage = createAction(actiontypes.CHANGE_MESSAGE);
export const sendMessage = createAction(actiontypes.SEND_MESSAGE);

// Global user status actions
export const changeUserStatusLogin = createAction(
  actiontypes.SET_USER_STATUS_LOGIN
);
export const changeUserStatusSignout = createAction(
  actiontypes.SET_USER_STATUS_SIGNOUT
);
