// Modal actions
export const OPEN_MODAL = "OPEN_MODAL";
export const CLOSE_MODAL = "CLOSE_MODAL";

// Sign up and log in form actions
export const SHOW_LOGIN = "SHOW_LOGIN";
export const RESET_FORM = "RESET_FORM";
export const SHOW_SIGNUP = "SHOW_SIGNUP";
export const CHANGE_EMAIL = "CHANGE_EMAIL";
export const CHANGE_PASSWORD = "CHANGE_PASSWORD";
export const SIGNOUT_USER = "SIGNOUT_USER";
export const SIGNOUT_USER_PENDING = "SIGNOUT_USER/pending";
export const SIGNOUT_USER_SUCCESS = "SIGNOUT_USER/fulfilled";
export const SIGNOUT_USER_ERROR = "SIGNOUT_USER/rejected";
export const LOGIN_USER = "LOGIN_USER";
export const LOGIN_USER_PENDING = "LOGIN_USER/pending";
export const LOGIN_USER_SUCCESS = "LOGIN_USER/fulfilled";
export const LOGIN_USER_ERROR = "LOGIN_USER/rejected";
export const SIGNUP_USER = "SIGNUP_USER";
export const SIGNUP_USER_PENDING = "SIGNUP_USER/pending";
export const SIGNUP_USER_SUCCESS = "SIGNUP_USER/fulfilled";
export const SIGNUP_USER_ERROR = "SIGNUP_USER/rejected";

// User settings actions
export const CHECK_TAG_UNIQUENESS = "CHECK_TAG_UNIQUENESS";
export const CHECK_TAG_UNIQUENESS_SUCCESS = "CHECK_TAG_UNIQUENESS/fulfilled";
export const CHANGE_NAME = "CHANGE_NAME";
export const CHANGE_PHONE = "CHANGE_PHONE";
export const CHANGE_TAG = "CHANGE_TAG";
export const CHANGE_INFO = "CHANGE_INFO";
export const UPLOAD_IMAGE = "UPLOAD_IMAGE";
export const POST_SETTINGS = "POST_SETTINGS";
export const POST_SETTINGS_SUCCESS = "POST_SETTINGS/fulfilled";
export const POST_SETTINGS_ERROR = "POST_SETTINGS/rejected";
export const POST_SETTINGS_WITH_PHOTO = "POST_SETTINGS_WITH_PHOTO";
export const POST_SETTINGS_WITH_PHOTO_SUCCESS =
  "POST_SETTINGS_WITH_PHOTO/fulfilled";
export const POST_SETTINGS_WITH_PHOTO_ERROR =
  "POST_SETTINGS_WITH_PHOTO/rejected";
export const FETCH_PROFILE_INFO = "FETCH_PROFILE_INFO";
export const FETCH_PROFILE_INFO_SUCCESS = "FETCH_PROFILE_INFO/fulfilled";
export const FETCH_PROFILE_INFO_ERROR = "FETCH_PROFILE_INFO/rejected";
export const CLEANUP_PROFILE_INFO = "CLEANUP_PROFILE_INFO";

//Friends reducer
export const UPDATE_FRIEND_TAG = "UPDATE_FRIEND_TAG";
export const CHECK_IF_TAG_EXISTS = "CHECK_IF_TAG_EXISTS";
export const CHECK_IF_TAG_EXISTS_SUCCESS = "CHECK_IF_TAG_EXISTS/fulfilled";
export const ADD_FRIEND = "ADD_FRIEND";
export const ADD_FRIEND_SUCCESS = "ADD_FRIEND/fulfilled";
export const FETCH_FRIENDS = "FETCH_FRIENDS";
export const FETCH_FRIENDS_SUCCESS = "FETCH_FRIENDS/fulfilled";
export const CLEANUP_FRIENDS = "CLEANUP_FRIENDS";

//Chats actions
export const FETCH_CHATS = "FETCH_CHATS";
export const FETCH_CHATS_SUCCESS = "FETCH_CHATS/fulfilled";
export const CHANGE_CHAT_NAME = "CHANGE_CHAT_NAME";
export const CLEANUP_CHATS = "CLEANUP_CHATS";
export const CHANGE_MESSAGE = "CHANGE_MESSAGE";
export const SEND_MESSAGE = "SEND_MESSAGE";

// Global user status actions
export const SET_USER_STATUS_LOGIN = "SET_USER_STATUS_LOGIN";
export const SET_USER_STATUS_SIGNOUT = "SET_USER_STATUS_SIGNOUT";
