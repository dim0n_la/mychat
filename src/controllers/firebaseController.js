import { auth } from "../services/firebaseAuth";
import storageRef from "../services/firebaseStorage";
import db, { addToArray } from "../services/firebaseDatabase";
import axios from "axios";
import * as firebase from "firebase/app";

export const userLogin = async (email, password) => {
  const user = await auth.signInWithEmailAndPassword(email, password);
  return user;
};

export const userSignup = async (email, password) => {
  const user = await auth.createUserWithEmailAndPassword(email, password);
  return user;
};

export const getCurrentUserID = () => {
  return auth.currentUser.uid;
};

export const userSignout = () => {
  auth.signOut();
};

export const setAuthStateListener = (callback) => {
  return auth.onAuthStateChanged(callback);
};

export const addUserInfo = async (uid, info) => {
  return await db.collection("users").doc(uid).set({ userData: info });
};

export const postUserSettings = async (settings) => {
  const result = await db
    .collection("users")
    .doc(getCurrentUserID())
    .update({ userData: settings });
  return result;
};

export const postUserTag = async (tag) => {
  const queryResults = await db.collection("tags").doc(tag).set({ tag: tag });
};

export const getUserInfo = async (email) => {
  const queryResults = await db
    .collection("users")
    .where("userData.email", "==", email)
    .get();
  const userInfo = [];
  queryResults.forEach((doc) => {
    userInfo.push(doc.data());
  });
  return userInfo[0].userData;
};

export const createChat = async (
  userOne,
  userOnePhotolink,
  userTwo,
  userTwoPhotolink
) => {
  const docData = {
    users: {
      userOne: {
        name: userOne,
        photoLink: userOnePhotolink,
      },
      userTwo: {
        name: userTwo,
        photoLink: userTwoPhotolink,
      },
    },
    messages: [],
  };
  const ref = await db.collection("chats").add(docData);
  await ref.update({ chatID: ref.id });
  return ref.id;
};

export const tagExists = async (tag) => {
  const queryResults = await db
    .collection("tags")
    .where("tag", "==", tag)
    .get();
  const result = queryResults.docs.map((doc) => doc.data());
  return result.length === 0;
};

export const putImage = async (photo) => {
  const childRef = storageRef.child(photo.name);
  const parentRef = storageRef.child(`images/${photo.name}`);
  const uploadTaskSnapshot = await parentRef.put(photo);
  const fullPath = uploadTaskSnapshot.metadata.fullPath;
  const downloadURL = await storageRef.child(fullPath).getDownloadURL();
  return downloadURL;
};

export const getImageFromStorage = async (URL) => {
  const file = await axios.get(URL);
  return file;
};

export const getUserByTag = async (tag) => {
  const queryResults = await db
    .collection("users")
    .where("userData.tag", "==", tag)
    .get();
  const user = queryResults.docs.map((doc) => doc.data());
  return {
    name: user[0].userData.name,
    tag: user[0].userData.tag,
    photoLink: user[0].userData.photoLink,
  };
};

export const addUserToFriendsList = async (user, chatID) => {
  let data = Object.assign(user, { chatID: chatID });
  const queryResults = await db
    .collection("users")
    .doc(getCurrentUserID())
    .update({
      friendsList: firebase.firestore.FieldValue.arrayUnion(data),
    });
  return data;
};

export const getUserFriends = async () => {
  const user = await getCurrentUserInfo();
  return user.friendsList;
};

export const getUserChats = async () => {
  const friendList = await getUserFriends();
  const chatIDs = friendList.map((friend) => {
    return friend.chatID;
  });
  const chats = await Promise.all(
    chatIDs.map(async (chatID) => {
      let doc = await db.collection("chats").doc(chatID).get();
      //todo: find out if I can better self-document this
      let data = doc.data();
      return data;
    })
  );
  return chats;
};

export const getCurrentUserInfo = async () => {
  const doc = await db.collection("users").doc(getCurrentUserID()).get();
  const user = doc.data();
  return user;
};
